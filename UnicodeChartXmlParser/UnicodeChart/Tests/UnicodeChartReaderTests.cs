﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace UnicodeChart.Tests {
    [TestFixture]
    class UnicodeChartReaderTests {
        [Test, Explicit]
        public void FlateXml() {
            var xmlFilePath = "c:/!RTL/ucd.all.flat.xml";
            var cmpFilePath = "c:/!RTL/ucd.all.flat.cxml";
            using (var file = File.OpenRead(xmlFilePath))
            using (var compressed = File.Open(cmpFilePath, FileMode.OpenOrCreate))
            using (var deflate = new DeflateStream(compressed, CompressionMode.Compress)) {
                file.CopyTo(deflate);
            }
        }
        [Test]
        public void CharInfoRTL() {
            using (var reader = new UnicodeChartReader()) {
                var enumerator = reader.CharInfos((ct, gc, bc) => {
                    return (ct == CharType.Char /*|| ct == CharType.Reserved || ct == CharType.Surrogate*/)
                        && (bc == BidiClass.AL || bc == BidiClass.R /*|| bc == BidiClass.RLE || bc == BidiClass.RLO || bc == BidiClass.RLI*/);
                });
                int i = 1;
                foreach (var range in enumerator.Select(e => { Debug.WriteLine(e); return e; }).GetCharCodeRanges().ToList()) {
                    Debug.Write($"new Range(0x{range.Start.ToString("x4")}, 0x{range.End.ToString("x4")}), ");
                    if (i++ % 3 == 0)
                        Debug.WriteLine("");
                }
            }
        }
        [Test]
        public void CharInfoLTR() {
            using (var reader = new UnicodeChartReader()) {
                var enumerator = reader.CharInfos((ct, gc, bc) => {
                    return (ct == CharType.Char /*|| ct == CharType.Reserved || ct == CharType.Surrogate*/)
                        && (bc == BidiClass.L);
                });
                int i = 1;
                foreach (var range in enumerator/*.Select(e => { Debug.WriteLine(e); return e; })*/.GetCharCodeRanges().ToList()) {
                    Debug.Write($"new Range(0x{range.Start.ToString("x4")}, 0x{range.End.ToString("x4")}), ");
                    if (i++ % 3 == 0)
                        Debug.WriteLine("");
                }
            }
        }
        [Test]
        public void Punctuation() {
            using (var reader = new UnicodeChartReader()) {
                var enumerator = reader.CharInfos((ct, gc, bc) => CharCategory.P.HasFlag(gc));

                foreach (var range in enumerator.Select(e => { Debug.WriteLine(e); return e; }).GetCharCodeRanges().ToList())
                    Debug.WriteLine(range);
            }
        }
        [Test]
        internal void SerializeBidiClass() {
            using (var reader = new UnicodeChartReader())
            using (var file = File.Create("c:/!RTL/bidiclasses.bin"))
            using (var writer = new BinaryWriter(file)) {
                var enumerator = reader.CharInfos((ct, gc, bc) => ct == CharType.Char);
                foreach (var c in enumerator) {
                    writer.Write(c.Code);
                    writer.Write((byte)c.BidiClass);
                }
            }
        }

        [Flags]
        enum CharFlags: byte {
            None = 0,
            RTL = 1,
            LTR = 2,
            Modifier = 4,
            TaiDistribute = 8,
            Distribute = 16,
        }

        [Test]
        public void SerializeAllChars_BidiClass_RTL_LTR() {
            var modifiers = new HashSet<int>();
            using (var reader = new UnicodeChartReader())
                foreach (var c in reader.CharInfos())
                    if (c.Category == CharCategory.Mn)
                        modifiers.Add(c.Code);

            using (var reader = new UnicodeChartReader())
            using (var file = File.Create("c:/!RTL/CharFlags.bin"))
            using (var writer = new BinaryWriter(file)) {
                foreach (var c in reader.CharInfos()) {
                    Assert.AreEqual(c.Code, file.Position);

                    var flags = CharFlags.None;
                    if (c.BidiClass.IsRtl())
                        flags |= CharFlags.RTL;
                    if (c.BidiClass.IsLtr())
                        flags |= CharFlags.LTR;
                    if (c.Category == CharCategory.Mn)
                        flags |= CharFlags.Modifier;
                    if(!c.Composition.IsEmpty && modifiers.Overlaps(c.Composition.Codes))
                        flags |= CharFlags.Modifier;

                    var block = Blocks.FindByCharCode(c.Code);
                    if (block.Distribute.HasFlag(Distribute.Tai))
                        flags |= CharFlags.TaiDistribute;
                    if (block.Distribute.HasFlag(Distribute.Other))
                        flags |= CharFlags.Distribute;

                    writer.Write((byte)flags);
                }
            }
        }
        [Test]
        public void MirroredPairs() {
            using (var reader = new UnicodeChartReader()) {
                var i = 0;
                foreach (var pair in reader.MirroredPairs()) {
                    if (i++ % 3 == 0)
                        Debug.WriteLine("");
                    Debug.Write(pair);
                }
            }
        }
        [Test]
        public void DocXRtlRunBidiLevels() {
            using (var reader = new UnicodeChartReader()) {
                var enumerator = reader.CharInfos((ct, gc, bc) =>
                    (
                        bc == BidiClass.NSM || bc == BidiClass.BN /*|| bc == BidiClass.ES || bc == BidiClass.CS /**/
                        || bc == BidiClass.B || bc == BidiClass.S || bc == BidiClass.WS || bc == BidiClass.ON
                    )
                    && (ct == CharType.Char || ct == CharType.NonCharacter)
                );
                var i = 0;
                foreach (var c in enumerator) {
                    Debug.Write($"'\\u{c.Code.ToString("X4")}',");
                    if (i++ % 8 == 0)
                        Debug.WriteLine("");
                }
            }
        }
        [Test]
        public void CharBlocks() {
            using (var reader = new UnicodeChartReader()) {
                foreach (var block in reader.Blocks())
                    Debug.WriteLine(block.ToCode());
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Xml;

namespace UnicodeChart {
    public static class DisposeExt {
        public static void SafeDispose<T>(ref T field) where T : class, IDisposable {
            if(field != null)
                field.Dispose();
            field = null;
        }
    }
    public static class XmlReaderExt {
        public static XmlReader ReadToElement(this XmlReader reader) {
            while(reader.Read())
                if(reader.NodeType == XmlNodeType.Element)
                    break;
            return reader;
        }
        public static XmlReader Skip(this XmlReader reader, params string[] names) {
            while(names.Contains(reader.Name))
                reader.ReadToElement();
            return reader;
        }
        public static XmlReader ThrowIfNameInvalid(this XmlReader reader, params string[] names) {
            if(names.Contains(reader.Name))
                return reader;
            throw new InvalidOperationException();
        }
    }

    public struct CodeRange {
        public int Start;
        public int End;
        public string Classes;
        public CodeRange(int start, int end, string classes) {
            Start = start;
            End = end;
            Classes = classes;
        }
        public override string ToString() {
            return $"({Start.ToString("X4")}, {End.ToString("X4")}, {Classes})";
        }
    }
    public static class UnicodeChartReaderExt {
        public static IEnumerable<CodeRange> GetCharCodeRanges(this IEnumerable<CharInfo> infos) {
            int? start = null;
            int? end = null;
            HashSet<BidiClass> classes = new HashSet<BidiClass>();
            foreach(var info in infos) {
                if(start == null)
                    start = info.Code;
                if(end == null)
                    end = info.Code;

                classes.Add(info.BidiClass);
                if(info.Code == end + 1)
                    end = info.Code;
                else {
                    yield return new CodeRange(start.Value, end.Value, string.Join(" ", classes));
                    start = end = info.Code;
                    classes.Clear();
                }
            }

            if(start != end)
                yield return new CodeRange(start.Value, end.Value, string.Join(" ", classes));
        }
        public static bool IsRtl(this BidiClass bc) {
            return bc == BidiClass.AL
                || bc == BidiClass.AN
                || bc == BidiClass.FSI
                || bc == BidiClass.PDF
                || bc == BidiClass.PDI
                || bc == BidiClass.R
                || bc == BidiClass.RLE
                || bc == BidiClass.RLI
                || bc == BidiClass.RLO;
        }
        public static bool IsLtr(this BidiClass bc) {
            return bc == BidiClass.L;
        }
    }

    public class ZipContainer: IDisposable {
        ZipArchive zipArchive;

        public ZipContainer(string fileName) {
            zipArchive = ZipFile.Open(fileName, ZipArchiveMode.Read);
        }
        public Stream GetEntryStream(string entryName) 
            => zipArchive.GetEntry(entryName).Open();

        public void Dispose() {
            DisposeExt.SafeDispose(ref zipArchive);
        }
    }
    public class DeflateContainer: IDisposable {
        DeflateStream deflateStream;
        public DeflateContainer(Stream resourceStream) {
            deflateStream = new DeflateStream(resourceStream, CompressionMode.Decompress);
        }
        public DeflateStream DeflateStream => deflateStream;

        public void Dispose() {
            DisposeExt.SafeDispose(ref deflateStream);
        }
    }

    public enum CharType: byte {
        Char, Reserved, Surrogate, NonCharacter
    }
    public enum BidiClass: byte {
        L, R, AL, EN, ES, ET, AN, CS, NSM, BN, B, S, WS, ON, LRE, LRO, RLE, RLO, PDF, LRI, RLI, FSI, PDI
    }
    [Flags]
    public enum CharCategory: uint {
        Lu = 0x1, // Uppercase_Letter    an uppercase letter
        Ll = 0x2, // Lowercase_Letter    a lowercase letter
        Lt = 0x4, // Titlecase_Letter    a digraphic character, with first part uppercase
        Lm = 0x8, // Modifier_Letter	a modifier letter
        Lo = 0x10, // Other_Letter    other letters, including syllables and ideographs
        Mn = 0x20, // Nonspacing_Mark a nonspacing combining mark (zero advance width)
        Mc = 0x40, // Spacing_Mark    a spacing combining mark (positive advance width)
        Me = 0x80, // Enclosing_Mark  an enclosing combining mark
        Nd = 0x100, // Decimal_Number  a decimal digit
        Nl = 0x200, // Letter_Number   a letterlike numeric character
        No = 0x400, // Other_Number    a numeric character of other type
        Pc = 0x800, // Connector_Punctuation   a connecting punctuation mark, like a tie
        Pd = 0x1000, // Dash_Punctuation    a dash or hyphen punctuation mark
        Ps = 0x2000, // Open_Punctuation    an opening punctuation mark (of a pair)
        Pe = 0x4000, // Close_Punctuation   a closing punctuation mark (of a pair)
        Pi = 0x8000, // Initial_Punctuation an initial quotation mark
        Pf = 0x10000, // Final_Punctuation   a final quotation mark
        Po = 0x20000, // Other_Punctuation   a punctuation mark of other type
        Sm = 0x40000, // Math_Symbol a symbol of mathematical use
        Sc = 0x80000, // Currency_Symbol a currency sign
        Sk = 0x100000, // Modifier_Symbol a non-letterlike modifier symbol
        So = 0x200000, // Other_Symbol    a symbol of other type
        Zs = 0x400000, // Space_Separator a space character (of various non-zero widths)
        Zl = 0x800000, // Line_Separator  U+2028 LINE SEPARATOR only
        Zp = 0x1000000, // Paragraph_Separator U+2029 PARAGRAPH SEPARATOR only
        Cc = 0x2000000, // Control a C0 or C1 control code
        Cf = 0x4000000, //  Format  a format control character
        Cs = 0x8000000, // Surrogate   a surrogate code point
        Co = 0x10000000, // Private_Use a private-use character
        Cn = 0x20000000, // Unassigned  a reserved unassigned code point or a noncharacter

        LC = Lu | Ll | Lt, // Cased_Letter
        L = Lu | Ll | Lt | Lm | Lo, // Letter
        M = Mn | Mc | Me, // Mark
        N = Nd | Nl | No, // Number
        P = Pc | Pd | Ps | Pe | Pi | Pf | Po, // Punctuation
        S = Sm | Sc | Sk | So, // Symbol
        Z = Zs | Zl | Zp, // Separator
        C = Cc | Cf | Cs | Co | Cn // Other
    }
    public enum BidiPairedBracketType { n, o, c }
    public enum PairedBracketType { None, Open, Close }
    public struct CharComposition {
        public static readonly CharComposition Empty = new CharComposition();
        public readonly int[] Codes;
        public CharComposition(string codes) {
            var splitted = codes.Split(' ');
            Codes = new int[splitted.Length];
            for (int i = 0; i < splitted.Length; i++)
                Codes[i] = int.Parse(splitted[i], System.Globalization.NumberStyles.AllowHexSpecifier);
        }
        public bool IsEmpty => Codes == null;
    }
    public struct CharInfo {
        public readonly int Code;
        public readonly CharType CharType;
        public readonly BidiClass BidiClass;
        public readonly CharCategory Category;
        public readonly CharComposition Composition;
        public CharInfo(CharType charType, string cp, CharCategory gc, BidiClass bc) {
            CharType = charType;
            Code = int.Parse(cp, System.Globalization.NumberStyles.AllowHexSpecifier);
            BidiClass = bc;
            Category = gc;
            Composition = CharComposition.Empty;
        }
        public CharInfo(CharType charType, int cp, CharCategory gc, BidiClass bc) {
            CharType = charType;
            Code = cp;
            BidiClass = bc;
            Category = gc;
            Composition = CharComposition.Empty;
        }
        private CharInfo(CharInfo info, string composition) {
            Code = info.Code;
            CharType = info.CharType;
            BidiClass = info.BidiClass;
            Category = info.Category;
            Composition = new CharComposition(composition);
        }
        public CharInfo WithComposition(string composition) => new CharInfo(this, composition);
        public override string ToString() => $"{Code.ToString("X4")} {CharType} gc={Category} bc={BidiClass}";
    }
    public struct MirroredPair {
        public readonly int Code;
        public readonly int Mirror;
        public readonly string Name;
        public readonly PairedBracketType PairedBracketType;
        public MirroredPair(string name, int code, int mirror, BidiPairedBracketType pairedBracketType) {
            Name = name;
            Code = code;
            Mirror = mirror;
            PairedBracketType = (PairedBracketType)pairedBracketType;
        }
        //public override string ToString() => $"{Code.ToString("X4")} bmg={Mirror.ToString("X4")} {Name}";
        public override string ToString() => $"{{ 0x{Code.ToString("X4")}, new Mirrored(0x{Mirror.ToString("X4")}, BidiPairedBracketType.{PairedBracketType}) }}, ";
    }
    public class UnicodeChartReader: IDisposable {
        const string UnicodeChartResourceName = "ucd.all.flat.cxml";

        XmlTextReader reader;
        DeflateContainer deflateContainer;
        string description;

        public UnicodeChartReader() {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = assembly.GetManifestResourceNames().First(r => r.EndsWith(UnicodeChartResourceName));
            var resourceStream = assembly.GetManifestResourceStream(resourceName);

            deflateContainer = new DeflateContainer(resourceStream);
            reader = new XmlTextReader(deflateContainer.DeflateStream);
            InitialRead();
        }

        public string Description => description;

        void InitialRead() {
            reader.ReadToElement().ThrowIfNameInvalid("ucd");
            reader.ReadToElement().ThrowIfNameInvalid("description");
            description = reader.ReadElementContentAsString();
            reader.ReadToElement().ThrowIfNameInvalid("repertoire");
        }   
        IEnumerable<CharInfo> GenerateInterval(CharType charType, CharCategory gc, BidiClass bc) {
            var cp = reader.GetAttribute("cp");           
            if(!string.IsNullOrEmpty(cp)) {
                yield return new CharInfo(charType, cp, gc, bc);
            } else {
                var start = int.Parse(reader.GetAttribute("first-cp"), System.Globalization.NumberStyles.AllowHexSpecifier);
                var end = int.Parse(reader.GetAttribute("last-cp"), System.Globalization.NumberStyles.AllowHexSpecifier);
                for(int i = start; i <= end; i++)
                    yield return new CharInfo(charType, i, gc, bc);
            }
        }
        public IEnumerable<CharInfo> CharInfos(Func<CharType, CharCategory, BidiClass, bool> predicate = null) {
            while(true) {
                reader.ReadToElement().Skip("name-alias");
                if(reader.Name == "blocks")
                    yield break;

                reader.ThrowIfNameInvalid("char", "reserved", "surrogate", "noncharacter");

                var charType = (CharType)Enum.Parse(typeof(CharType), reader.Name, true);
                var bc = (BidiClass)Enum.Parse(typeof(BidiClass), reader.GetAttribute("bc"));
                var gc = (CharCategory)Enum.Parse(typeof(CharCategory), reader.GetAttribute("gc"));
                var dt = reader.GetAttribute("dt");
                var dm = reader.GetAttribute("dm");

                if (predicate == null || predicate(charType, gc, bc))
                    foreach(var info in GenerateInterval(charType, gc, bc))
                        yield return dt == "com" ? info.WithComposition(dm) : info;
            }
        }
        public IEnumerable<MirroredPair> MirroredPairs() {
            while(true) {
                reader.ReadToElement().Skip("name-alias");
                if(reader.Name == "blocks")
                    yield break;

                reader.ThrowIfNameInvalid("char", "reserved", "surrogate", "noncharacter");
                var Bidi_M = reader.GetAttribute("Bidi_M");
                if(Bidi_M != "Y")
                    continue;

                var cp = reader.GetAttribute("cp");
                if(string.IsNullOrEmpty(cp))
                    continue;
                
                int bmg;
                if(int.TryParse(reader.GetAttribute("bmg"), System.Globalization.NumberStyles.AllowHexSpecifier, null, out bmg)) {
                    var bpt = (BidiPairedBracketType)Enum.Parse(typeof(BidiPairedBracketType), reader.GetAttribute("bpt"));
                    var code = int.Parse(cp, System.Globalization.NumberStyles.AllowHexSpecifier);
                    var name = reader.GetAttribute("na");
                    yield return new MirroredPair(name, code, bmg, bpt);
                }
            }
        }
        public IEnumerable<Block> Blocks() {
            while (true) {
                reader.ReadToElement().Skip("name-alias", "char", "reserved", "surrogate", "noncharacter", "blocks");
                if (reader.Name == "named-sequences")
                    yield break;

                reader.ThrowIfNameInvalid("block");
                var start = int.Parse(reader.GetAttribute("first-cp"), System.Globalization.NumberStyles.AllowHexSpecifier);
                var end = int.Parse(reader.GetAttribute("last-cp"), System.Globalization.NumberStyles.AllowHexSpecifier);
                var name = reader.GetAttribute("name");
                yield return new Block(start, end, name);
            }
        }
        public void Dispose() {
            DisposeExt.SafeDispose(ref reader);
            DisposeExt.SafeDispose(ref deflateContainer);
        }
    }
}

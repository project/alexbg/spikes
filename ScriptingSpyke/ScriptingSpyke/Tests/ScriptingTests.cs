﻿using NUnit.Framework;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using System.Threading.Tasks;
using System.Reflection;
using System.Linq;
using System;
using System.IO;
using Microsoft.CodeAnalysis.Emit;
using Microsoft.CodeAnalysis.Scripting;
using Microsoft.CodeAnalysis;

namespace ScriptingSpyke.Tests {
    [TestFixture]
    public class ScriptingTests {
        [Test]
        public async Task Variable() {
            var script = CSharpScript.Create("var x = 1;");
            var diagnostics = script.Compile();
            var state = await script.RunAsync();
            Assert.AreEqual(1, state.Variables.Length);
            state.Variables[0].AssertVariable("x", typeof(int), 1);
        }

        public class GlobalObject1 {
            public class Persistent: System.Attribute { }
        }
        public class GlobalObject2 {
            public class Persistent: System.Attribute { }
            [Persistent]
            public class TestClass {
                public string Name = "TestClassName";
                public int Count = 1;
            }
            public object Result;
        }
        [Test]
        public async Task ClassDeclaration1() {
            var script = CSharpScript.Create(
@"[Persistent]
public class TestClass {
    public string name = ""TestClassName"";
    public int count = 1;
}

var x = new TestClass();",
            globalsType: typeof(GlobalObject1));
            var diagonstics = script.Compile();
            var state = await script.RunAsync(globals: new GlobalObject1());
            var value = state.Variables[0].AssertVariable("x", "Submission#0+TestClass");
            var type = value.GetType();
            Assert.IsInstanceOf<GlobalObject1.Persistent>(type.GetCustomAttributes().Single());
        }
        [Test]
        public async Task ClassDeclaration2() {
            var script = CSharpScript.Create(
@"Result = new TestClass();",
            globalsType: typeof(GlobalObject2));
            var diagonstics = script.Compile();
            var globalObject = new GlobalObject2();
            var state = await script.RunAsync(globals: globalObject);
            Assert.IsInstanceOf<GlobalObject2.TestClass>(globalObject.Result);

            var type = globalObject.Result.GetType();
            Assert.IsInstanceOf<GlobalObject2.Persistent>(type.GetCustomAttributes().Single());
        }
        [Test]
        public void Compilation() {
            var script = CSharpScript.Create(
                @"var x = ""test"";",
                globalsType: typeof(GlobalObject1));
            var diagonstics = script.Compile();
            var compilation = script.GetCompilation();
            using(var stream = File.Create(@"scripting.dll"))
            using(var pdbstream = File.Create(@"scripting.pdb")) {
                var result = compilation.Emit(stream, pdbstream);
                Assert.IsTrue(result.Success);
            }
        }        
        [Test]
        public void ExternalScriptWithDebugSymbols() {
            var script = CSharpScript.Create(@"#load ""script.cs""",
                options: ScriptOptions.Default.WithSourceResolver(new SourceFileResolver(
                    new string[] { AppDomain.CurrentDomain.BaseDirectory + "TestResourses" },
                    AppDomain.CurrentDomain.BaseDirectory
                )),
                globalsType: typeof(GlobalObject1)
            );
            var diagonstics = script.Compile();
            var compilation = script.GetCompilation();
            using(var stream = File.Create(@"scripting.dll"))
            using(var pdbstream = File.Create(@"scripting.pdb")) {
                var result = compilation.Emit(stream, pdbstream);
                Assert.IsTrue(result.Success);
            }
        }
        [Test]
        public async Task DebugScript() {
            var script = CSharpScript.Create(@"#load ""script.cs""",
                options: ScriptOptions.Default.WithSourceResolver(new SourceFileResolver(
                    new string[] { AppDomain.CurrentDomain.BaseDirectory + "TestResourses" }, 
                    AppDomain.CurrentDomain.BaseDirectory
                )),
                globalsType: typeof(GlobalObject1));
            var diagonstics = script.Compile();
            var compilation = script.GetCompilation();
            using(var stream = File.Create(@"scripting.dll"))
            using(var pdbstream = File.Create(@"scripting.pdb")) {
                var result = compilation.Emit(stream, pdbstream);
                Assert.IsTrue(result.Success);
            }
            var globalObject = new GlobalObject1();
            var state = await script.RunAsync(globals: globalObject);
            Assert.IsInstanceOf<string>(state.ReturnValue);
            Assert.AreEqual("Test", state.ReturnValue);
        }
        [Test]
        public async Task ReflectionAppDomain() {
            var script = CSharpScript.Create(
@"Result = System.AppDomain.CurrentDomain;",
            globalsType: typeof(GlobalObject2));
            var globalObject = new GlobalObject2();
            var state = await script.RunAsync(globals: globalObject);
        }
    }
}

﻿using System;
using Microsoft.CodeAnalysis.Scripting;
using NUnit.Framework;

namespace ScriptingSpyke.Tests {
    static class TestExt {
        public static object AssertVariable(this ScriptVariable variable, string name, string typeName) {
            Assert.AreEqual(name, variable.Name);
            Assert.AreEqual(typeName, variable.Type.FullName);
            return variable.Value;
        }
        public static object AssertVariable(this ScriptVariable variable, string name, Type type) {
            Assert.AreEqual(name, variable.Name);
            Assert.AreEqual(type, variable.Type);
            return variable.Value;
        }
        public static void AssertVariable(this ScriptVariable variable, string name, Type type, object value = null) {
            variable.AssertVariable(name, type);
            Assert.AreEqual(value, variable.Value);
        }
    }
}

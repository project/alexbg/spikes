﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Media.Imaging;
using DevExpress.Utils;
using DevExpress.Utils.DirectXPaint;
using NUnit.Framework;
namespace DirectWrite {
    [TestFixture]
    public class DirectWriteTests {
        unsafe class TextAnalysisSource : IDWriteTextAnalysisSource, IDWriteTextAnalysisSink, IDisposable {
            string text;
            string locale = "en-us";

            public TextAnalysisSource(string locale, string text) {
                this.text = text;
                this.locale = locale;
            }

            public uint TextLength { get { return (uint)text.Length; } }
            public void GetTextAtPosition(uint textPosition, out string textString, out uint textLength) {
                textString = text.Substring((int)textPosition);
                textLength = (uint)(text.Length - textPosition);
            }
            public void GetTextBeforePosition(uint textPosition, out string textString, out uint textLength) {
                throw new NotImplementedException();
            }
            public DWRITE_READING_DIRECTION GetParagraphReadingDirection() {
                return DWRITE_READING_DIRECTION.LEFT_TO_RIGHT;
            }
            public void GetLocaleName(uint textPosition, out uint textLength, out string localeName) {
                textLength = (uint)(text.Length - textPosition);
                localeName = locale;
            }
            public void GetNumberSubstitution(uint textPosition, out uint textLength, out IDWriteNumberSubstitution numberSubstitution) {
                numberSubstitution = null;
                textLength = (uint)(text.Length - textPosition);
            }
            public void Dispose() {
            }

            Dictionary<string, List<object>> result = new Dictionary<string, List<object>>();
            void AddResult(string name, object value) {
                List<object> list = null;
                if(!result.TryGetValue(name, out list))
                    list = result[name] = new List<object>();
                list.Add(value);
            }
            public void SetScriptAnalysis(int textPosition, int textLength, ref DWRITE_SCRIPT_ANALYSIS scriptAnalysis) {
                AddResult("SetScriptAnalysis", Tuple.Create(textPosition, textLength, scriptAnalysis));
            }
            public void SetLineBreakpoints(int textPosition, int textLength, DWRITE_LINE_BREAKPOINT[] lineBreakpoints) {
                AddResult("SetLineBreakpoints", Tuple.Create(textPosition, textLength, lineBreakpoints));
            }
            public void SetBidiLevel(int textPosition, int textLength, byte explicitLevel, byte resolvedLevel) {
                AddResult("SetBidiLevel", Tuple.Create(textPosition, textLength, explicitLevel, resolvedLevel, text.Substring((int)textPosition, (int)textLength)));
            }
            public void SetNumberSubstitution(int textPosition, int textLength, IDWriteNumberSubstitution numberSubstitution) {
                throw new NotImplementedException();
            }
        }
        class GdiTextRenderer: IDWriteTextRenderer {
            IDWriteBitmapRenderTarget target;
            IDWriteRenderingParams renderingParams;
            public GdiTextRenderer(IDWriteBitmapRenderTarget target, IDWriteRenderingParams renderingParams) {
                this.target = target;
                this.renderingParams = renderingParams;
            }
            public void IsPixelSnappingDisabled(IntPtr clientDrawingContext, [Out] out bool isDisabled) {
                isDisabled = false;
            }
            public void GetCurrentTransform(IntPtr clientDrawingContext, [Out] out DWRITE_MATRIX transform) {
                target.GetCurrentTransform(out transform);
            }
            public void GetPixelsPerDip(IntPtr clientDrawingContext, [Out] out float pixelsPerDip) {
                pixelsPerDip = target.GetPixelsPerDip();
            }
            public void DrawGlyphRun(IntPtr clientDrawingContext, float baselineOriginX, float baselineOriginY, 
                DWRITE_MEASURING_MODE measuringMode, ref DWRITE_GLYPH_RUN glyphRun, ref DWRITE_GLYPH_RUN_DESCRIPTION glyphRunDescription, 
                [MarshalAs(UnmanagedType.IUnknown)] object clientDrawingEffect) {

                DWRITE_RECT rect;
                IDWriteRenderingParams renderingParams = null;
                target.DrawGlyphRun(baselineOriginX, baselineOriginY, measuringMode, glyphRun, renderingParams, 0, out rect);
            }
            public void DrawUnderline(IntPtr clientDrawingContext, float baselineOriginX, float baselineOriginY, ref DWRITE_UNDERLINE underline, [MarshalAs(UnmanagedType.IUnknown)] object clientDrawingEffect) {
                throw new NotImplementedException();
            }
            public void DrawStrikethrough(IntPtr clientDrawingContext, float baselineOriginX, float baselineOriginY, ref DWRITE_STRIKETHROUGH strikethrough, [MarshalAs(UnmanagedType.IUnknown)] object clientDrawingEffect) {
                throw new NotImplementedException();
            }
            public void DrawInlineObject(IntPtr clientDrawingContext, float originX, float originY, IDWriteInlineObject inlineObject, bool isSideways, bool isRightToLeft, [MarshalAs(UnmanagedType.IUnknown)] object clientDrawingEffect) {
                throw new NotImplementedException();
            }
        }
        
        void RunInSTA(Action action) {
            var thread = new Thread(new ThreadStart(action));
            thread.IsBackground = true;
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();
        }

        //[TestCase("en-us", "test text\rTest\x00ADcatalog")]
        [TestCase("en-us", "ان عدة الشهور عند الله اثنا عشر شهرا في كتاب الله يوم خلق السماوات والارض منها اربعة حرم ذلك الدين القيم فلاتظلموا فيهن انفسكم وقاتلوا المشركين كافة كما يقاتلونكم كافة واعلموا ان الله مع المتقين")]
        [TestCase("ar-eg", "ان عدة الشهور عند الله اثنا عشر شهرا في كتاب الله يوم خلق السماوات والارض منها اربعة حرم ذلك الدين القيم فلاتظلموا فيهن انفسكم وقاتلوا المشركين كافة كما يقاتلونكم كافة واعلموا ان الله مع المتقين")]
        //[TestCase("ar-eg", "عندما يريد العالم أن ‪يتكلّم ‬ ، فهو يتحدّث بلغة يونيكود. تسجّل الآن لحضور المؤتمر الدولي العاشر ليونيكود (Unicode Conference)، الذي سيعقد في 10-12 آذار 1997 بمدينة مَايِنْتْس، ألمانيا. و سيجمع المؤتمر بين خبراء من كافة قطاعات الصناعة على الشبكة العالمية انترنيت ويونيكود، حيث ستتم، على الصعيدين الدولي والمحلي على حد سواء مناقشة سبل استخدام يونكود في النظم القائمة وفيما يخص التطبيقات الحاسوبية، الخطوط، تصميم النصوص والحوسبة")]
        public void TextAnalyze(string locale, string text) {
            IDWriteFactory factory;
            DWriteNativeMethods.DWriteCreateFactory(DWriteFactoryType.Isolated, typeof(IDWriteFactory).GUID, out factory);

            IDWriteTextAnalyzer textAnalyzer;
            factory.CreateTextAnalyzer(out textAnalyzer);

            using(var source = new TextAnalysisSource(locale, text)) {
                textAnalyzer.AnalyzeLineBreakpoints(source, 0, source.TextLength, source);
                textAnalyzer.AnalyzeBidi(source, 0, source.TextLength, source);
                textAnalyzer.AnalyzeScript(source, 0, source.TextLength, source);
                textAnalyzer.AnalyzeNumberSubstitution(source, 0, source.TextLength, source);
            }
        }

        [TestCase("en-us", "test text\rTest\x00ADcatalog")]
        [TestCase("ar-eg", "عندما يريد العالم أن ‪يتكلّم ‬ ، فهو يتحدّث بلغة يونيكود. تسجّل الآن لحضور المؤتمر الدولي العاشر ليونيكود (Unicode Conference)، الذي سيعقد في 10-12 آذار 1997 بمدينة مَايِنْتْس، ألمانيا. و سيجمع المؤتمر بين خبراء من كافة قطاعات الصناعة على الشبكة العالمية انترنيت ويونيكود، حيث ستتم، على الصعيدين الدولي والمحلي على حد سواء مناقشة سبل استخدام يونكود في النظم القائمة وفيما يخص التطبيقات الحاسوبية، الخطوط، تصميم النصوص والحوسبة")]
        public void RenderTarget(string locale, string text) {
            uint w = 300;
            uint h = 300;
            var font = new Font("Sergoe UI", 12);
            var textLayout = DWriteProvider.Default.CreateTextLayout(text, font, StringFormatInfo.GenericTypographics, w, h);
            textLayout.SetReadingDirection(locale == "en-us" ? DWRITE_READING_DIRECTION.LEFT_TO_RIGHT : DWRITE_READING_DIRECTION.RIGHT_TO_LEFT);

            var dfactory = D2DFunctions.CreateFactory();
            var wicFactory = (IWICImagingFactory)new WICImagingFactory();
            var wicBitmap = wicFactory.CreateBitmap(w, h, ref WICPixelFormatGUID.WICPixelFormat32bppPBGRA, WICBitmapCreateCacheOption.WICBitmapCacheOnLoad);

            D2D1_RENDER_TARGET_PROPERTIES props = new D2D1_RENDER_TARGET_PROPERTIES();
            var renderTarget = dfactory.CreateWicBitmapRenderTarget(wicBitmap, ref props);
            try {
                renderTarget.BeginDraw();
                ID2D1SolidColorBrush brush;
                renderTarget.CreateSolidColorBrush(new D2D_COLOR_F(1, 0, 0, 1), new D2D1_BRUSH_PROPERTIES() { opacity = 1 }, out brush);
                renderTarget.DrawTextLayout(new D2D_POINT_2F(10, 10), textLayout, (ID2D1Brush)brush, D2D1_DRAW_TEXT_OPTIONS.CLIP);                    
            } finally {
                ulong tag1, tag2;
                renderTarget.EndDraw(out tag1, out tag2);
            }

            using(var bmp = new Bitmap(300, 300, PixelFormat.Format32bppArgb)) {
                var rect = new WICRect() { X = 0, Y = 0, Width = (int)w, Height = (int)h };
                var bmpLock = bmp.LockBits(new Rectangle(0, 0, (int)w, (int)h), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
                wicBitmap.CopyPixels(rect, (uint)bmpLock.Stride, (uint)bmpLock.Stride * h, bmpLock.Scan0);
                bmp.Save($"c:/bmp-{locale}.png", ImageFormat.Png);
            }
        }

        [TestCase]
        public void DrawGlyphRun() {

        }

    }
}
